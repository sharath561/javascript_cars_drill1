// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function getCarYears(data) {
  let yearsData = [];

  for (let cars = 0; cars < data.length; cars++) {
    yearsData.push(data[cars].car_year);
  }

  return yearsData;
}

module.exports = getCarYears;
