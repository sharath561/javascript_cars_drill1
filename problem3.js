// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortedAlphabetical(data) {
  let sortData = [];

  for (let cars = 0; cars < data.length; cars++) {
    sortData.push(data[cars].car_model);
  }

  sortData.sort();
  return sortData;
}

module.exports = sortedAlphabetical;
