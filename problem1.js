// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//Car 33 is a *car year goes here* *car make goes here* *car model goes here

function recall_callInformation_33(data) {
  for (let cars = 0; cars < data.length; cars++) {
    if (data[cars].id == 33) {
      return `Car 33 is a ${data[cars].car_year} ${data[cars].car_make} ${data[cars].car_model}`;
    }
  }
}

module.exports = recall_callInformation_33;
