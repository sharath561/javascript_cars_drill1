// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function BmwAndAudi(data) {
  const Bmw_Audi_Data = [];

  for (let cars = 0; cars < data.length; cars++) {
    if (data[cars].car_make == "BMW" || data[cars].car_make == "Audi") {
      Bmw_Audi_Data.push(JSON.stringify(data[cars]));
    }
  }

  return Bmw_Audi_Data;
}

module.exports = BmwAndAudi;
