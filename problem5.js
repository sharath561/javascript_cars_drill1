const getCarYears = require("./problem4");
const inventory = require("./inventory")
// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
function oldCarsData() {
  let data = getCarYears(inventory);

  const oldYearsData = [];

  for (let cars = 0; cars < data.length; cars++) {
    if (data[cars] < 2000) {
      oldYearsData.push(data[cars]);
    }
  }

  return oldYearsData;
}

module.exports = oldCarsData;
