// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
//Last car is a *car make goes here* *car model goes here*"

function lastCarData(data, index) {
  if (index >= 0) {
    return `Last car is a ${data[index].car_make} ${data[index].car_model}`;
  }
}

module.exports = lastCarData;
